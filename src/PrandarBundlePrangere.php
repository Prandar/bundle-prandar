<?php

namespace App\Prandar\BundlePrangere\src;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use function dirname;

class PrandarBundlePrangere extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
