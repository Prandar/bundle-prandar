<?php

namespace App\Prandar\BundlePrangere\src;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class to show what's inside a CSV in a Table format
 */
class ShowCsvTable extends AbstractShowCsv
{
    /**
     * @param OutputInterface $output
     */
    public function renderTable(OutputInterface $output): void
    {
        // format data to be readable
        /** @var $rowsData array */
        $rowsData = $this->formatColumn();

        // create command table output
        $table = new Table($output);
        $table
            ->setHeaders(AbstractShowCsv::ARRAY_HEADER)
            ->setRows($rowsData)
        ;
        $table->render();
    }
}
